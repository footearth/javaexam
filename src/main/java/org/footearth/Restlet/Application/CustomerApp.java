package org.footearth.Restlet.Application;

import org.footearth.Restlet.Resource.CustomerRes;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class CustomerApp extends Application {

	@Override
	public Restlet createInboundRoot() {
		
		Router router = new Router(getContext());
		router.attach("", CustomerRes.class);
		return router;

	}

}
