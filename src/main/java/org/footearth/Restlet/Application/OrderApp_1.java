package org.footearth.Restlet.Application;

import org.footearth.Restlet.Resource.CustomerRes;
import org.footearth.Restlet.Resource.OrderRes;
import org.footearth.util.RestletServer;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class OrderApp_1 extends Application {

	@Override
	public Restlet createInboundRoot() {

		Router router = new Router(getContext());
		router.attach("/orders/{orderId}/{subOrderId}", OrderRes.class);
		router.attach("/customers/{custId}", CustomerRes.class);
		return router;

	}

	public static void main(String[] args) {

		RestletServer.testApplication(8080, new OrderApp_1());

	}

}
