package org.footearth.Restlet.Application;

import org.footearth.Restlet.Resource.OrderRes;
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class OrderApp_2 extends Application {

	@Override
	public Restlet createInboundRoot() {
		
		Router router = new Router(getContext());
		router.attach("", OrderRes.class);
		return router;

	}

}
