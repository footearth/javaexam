package org.footearth.Restlet.Resource;

import org.footearth.util.RestletClient;
import org.footearth.util.RestletServer;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class CustomerRes extends ServerResource {

	String customerId = "";

	@Override
	protected void doInit() throws ResourceException {
		this.customerId = (String) getRequest().getAttributes().get("custId");
	}

	@Get
	public String represent() {
		return "get customer id :" + customerId;
	}

	public static void main(String[] args) {
		RestletServer.testResource(8080, "/customers/{custId}",
				CustomerRes.class);
		RestletClient.prtResource(8080, "/customers/2");
	}
}
