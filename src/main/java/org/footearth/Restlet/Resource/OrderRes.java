package org.footearth.Restlet.Resource;

import org.footearth.util.RestletClient;
import org.footearth.util.RestletServer;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class OrderRes extends ServerResource {

	String orderId = "";
	String subOrderId = "";

	@Override
	protected void doInit() throws ResourceException {
		this.orderId = (String) getRequest().getAttributes().get("orderId");
		this.subOrderId = (String) getRequest().getAttributes().get(
				"subOrderId");
	}

	@Get
	public String represent() {
		return "the order id is : " + orderId + " and the sub order id is : "
				+ subOrderId;
	}

	public static void main(String[] args) {
		RestletServer.testResource(8080, "/orders/{orderId}/{subOrderId}",
				OrderRes.class);
		RestletClient.prtResource(8080, "/orders/2/1");
	}
}
