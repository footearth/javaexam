package org.footearth.Restlet.Component;

import org.footearth.Restlet.Application.CustomerApp;
import org.footearth.Restlet.Application.OrderApp_2;
import org.footearth.util.RestletServer;
import org.restlet.Component;

public class OrderCom extends Component {

	public OrderCom() {
		super();

		getDefaultHost().attach("/orders/{orderId}/{subOrderId}",
				new OrderApp_2());

		getDefaultHost().attach("/customers/{custId}", new CustomerApp());
	}

	public static void main(String[] args) {

		RestletServer.testComponent(8080, new OrderCom());

	}

}
