package org.footearth.util;

import java.util.Map;

import org.footearth.Restlet.Application.OrderApp_1;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;
import org.restlet.resource.ServerResource;

public class RestletServer {

	public static boolean startFileServer(int Port, final String filePath) {

		boolean retCode = false;

		startFileServer(Port, "", filePath);

		retCode = true;
		return retCode;
	}

	public static boolean startFileServer(int Port, String Path,
			final String filePath) {
		boolean retCode = false;

		// Create a component
		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Port);
		component.getClients().add(Protocol.FILE);

		// Create an application
		Application application = new Application() {

			@Override
			public Restlet createInboundRoot() {
				return new Directory(getContext(), "file:///" + filePath);
			}
		};

		// Attach the application to the component and start it
		component.getDefaultHost().attach(Path, application);
		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean testResource(int Port,
			Class<? extends ServerResource> ResourceClass) {
		boolean retCode = false;

		testResource(Port, "", ResourceClass);

		retCode = true;
		return retCode;
	}

	public static boolean testResource(int Port, String Path,
			Class<? extends ServerResource> ResourceClass) {
		boolean retCode = false;

		// Create a new Component.
		Component component = new Component();

		// Add a new HTTP server listening on port 8182.
		component.getServers().add(Protocol.HTTP, Port);

		component.getDefaultHost().attach(Path, ResourceClass);

		// Start the component.
		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean startResource(int Port,
			Map<String, Class<? extends ServerResource>> ResourceMap) {

		boolean retCode = false;

		// Create a new Component.
		Component component = new Component();

		// Add a new HTTP server listening on port 8182.
		component.getServers().add(Protocol.HTTP, Port);

		for (Map.Entry<String, Class<? extends ServerResource>> entry : ResourceMap
				.entrySet()) {
			String ResourcePath = entry.getKey();
			Class<? extends ServerResource> ResourceClass = entry.getValue();
			// Attach the sample application.
			component.getDefaultHost().attach(ResourcePath, ResourceClass);
		}

		// Start the component.
		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean testApplication(int Port, Application app) {

		boolean retCode = false;

		testApplication(Port, "", app);

		retCode = true;
		return retCode;
	}

	public static boolean testApplication(int Port, String Path, Application app) {

		boolean retCode = false;

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Port);

		component.getDefaultHost().attach(Path, app);

		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean startApplication(int Port,
			Map<String, Application> appMap) {

		boolean retCode = false;

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Port);

		for (Map.Entry<String, Application> appEty : appMap.entrySet()) {
			String CurPath = appEty.getKey();
			Application CurApp = appEty.getValue();
			component.getDefaultHost().attach(CurPath, CurApp);
		}

		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean testComponent(int Port, Component com) {

		boolean retCode = false;

		testComponent(Port, "", com);

		retCode = true;
		return retCode;
	}

	public static boolean testComponent(int Port, String Path, Component com) {

		boolean retCode = false;

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Port);

		component.getDefaultHost().attach(Path, com.getDefaultHost());

		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}

	public static boolean startComponent(int Port, Map<String, Component> comMap) {

		boolean retCode = false;

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Port);

		for (Map.Entry<String, Component> comEty : comMap.entrySet()) {
			String CurPath = comEty.getKey();
			Component CurCom = comEty.getValue();
			component.getDefaultHost().attach(CurPath, CurCom.getDefaultHost());
		}

		try {
			component.start();
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		retCode = true;
		return retCode;
	}
}
