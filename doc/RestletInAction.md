Restlet in Action（Restlet 实战）

	PART 1 GETTING STARTED（Restlet 入门）
	
		1 Introducing the Restlet Framework（Restlet 框架简介）
			1.1 Hello World（芝麻开门）
			1.2 Overview of the Restlet Framework（纵览）
			1.3 Summary（回顾）
			
		2 Beginning a Restlet application（创建 Restlet 应用）
			2.1 The purpose of Restlet applications（Restlet 的作用）
			2.2 The structure of Restlet applications（Restlet 应用的结构）
			2.3 Setting up a Restlet applications（创建一个 Restlet 应用）
			2.4 The Restlet routing system（Restlet 路由系统）
			2.5 Using Restlet resources in an application（使用 Restlet 应用访问URL资源）
			2.6 Summay（回顾）
			
		3 Deploying a Restlet application（部署 Restlet 应用）
		
	PART 2 GETTING READY TO ROLL OUT（进阶使用）
	
		4 Producing and consuming Restlet representations（生产消费详解）
			4.1 Overview of representations（概览）
			4.2 Producing and consuming XML representations（XML描述）
			4.3 Producing and consuming JSON representations（JSON描述）
			4.4 Applying template representations（应用模板）
			4.5 Content negotiation（内容协商）
			4.6 Summay（回顾）
			
		5 Securing a Restlet application（安全问题）
			5.1 Ensuring transport confidentiality and integrity
			5.2 Authenticating users
			5.3 Assigning roles to authenticated users
			5.4 Authorizing user actions
			5.5 Ensuring end-to-end integrity of data
			5.6 Summay（回顾）
			
		6 Documenting and versionning a Restlet application（文档和版本）
			6.1 The purpose of documentation and versioning
			6.2 Introducing WADL
			6.3 The WadlApplication class
			6.4 The WadlServerResource class
			6.5 Automatic conversion to HTML
			6.6 Summary（回顾）
			
		7 Enhancing a Restlet application with recipes and best practicea（更好的使用进阶配置）
			7.1 Handling common web elements
			7.2 Dealing with Atom and RSS feeds
			7.3 Redirecting client calls
			7.4 Improving performances
			7.5 Modularizing large applications
			7.6 Persisting resources state
			7.7 Summary（回顾）
			
	PART 3 FURTHER USE POSSIBILITES（其他应用）
	
		8 Using Restlet with cloud platforms（在云平台使用）
			8.1 Restlet main benefits in the cloud
			8.2 Deployment in Google App Engine
			8.3 Deployment in Amazon Elastic Beanstalk
			8.4 Deployment in Windows Azure
			8.5 Accessing web APIs from GAE
			8.6 Accessing OData services
			8.7 Accessing Amazon S3 resources
			8.8 Accessing Azure services
			8.9 Accessing intranet resources with Restlet's SDC
			8.10 Summary（回顾）
			
		9 Using Restlet in browsers and mobile devices（在浏览器或移动设备上使用）
			9.1 Understanding GWT
			9.2 The Restlet edition for GWT
			9.3 Server-side GWT extension
			9.4 Understanding Android
			9.5 The Restlet edition for Android
			9.6 Summary（回顾）
			
		10 Embracing hypermedia and the Semantic Web（超媒体和语义网）
			10.1 Hypermedia as the engine of RESTful web APIs
			10.2 The Semantic Web with Linked Data
			10.3 Exposing and consuming Linked Data with Restlet
			10.4 Summary（回顾）
			
		11 The future of Restlet（日后打算）
			11.1 Evolution of HTTP and the rise of SPDY
			11.2 The Restlet roadmap
			11.3 Restlet community
			11.4 Summary（回顾）